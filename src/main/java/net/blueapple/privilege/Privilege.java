package net.blueapple.privilege;

import net.blueapple.role.Role;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Lithium on 6/22/2017.
 */
@Entity
public class Privilege {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "privilege_id")
    private long id;
    private String name;

    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "privileges")
    private Set<Role> roles;

    public Privilege(String name) {
        this.name = name;
        this.roles=new HashSet<>();
    }

    public Privilege() {
        this.roles=new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}

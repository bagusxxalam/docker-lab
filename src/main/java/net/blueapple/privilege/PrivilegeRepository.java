package net.blueapple.privilege;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Lithium on 6/22/2017.
 */
public interface PrivilegeRepository extends JpaRepository<Privilege,Long> {

    Privilege findByName(String name);
}

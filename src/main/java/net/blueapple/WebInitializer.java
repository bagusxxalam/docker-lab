package net.blueapple;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Created by Lithium on 4/6/2017.
 * i create this so this app can be deployed to tomcat server to be clear
 * try to run this app without this class using run configuration tomcat server
 */
public class WebInitializer extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DockerLabApplication.class);
    }
}

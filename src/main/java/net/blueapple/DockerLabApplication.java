package net.blueapple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan(basePackages = "net.blueapple")
//@EnableJpaRepositories(basePackages = "net.blueapple")
public class DockerLabApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerLabApplication.class, args);
	}
}

package net.blueapple.user;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Size;

/**
 * Created by Lithium on 6/22/2017.
 */
public class UserDto {
    @Size(min = 4,message = "Username min 4 chars")
    private String username;
    @Size(min = 4,message = "Password min 4 chars")
    private String password;
    @Email
    private String email;

    public UserDto(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public UserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

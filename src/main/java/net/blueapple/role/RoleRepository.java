package net.blueapple.role;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Lithium on 6/22/2017.
 */
public interface RoleRepository extends JpaRepository<Role,Long> {

    Role findByName(String name);
}

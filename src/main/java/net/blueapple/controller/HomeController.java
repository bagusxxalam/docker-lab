package net.blueapple.controller;

import net.blueapple.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Lithium on 6/22/2017.
 */
@Controller
public class HomeController {
    private UserService userService;

    @Autowired
    public HomeController(UserService userService) {
        this.userService=userService;
    }

    @RequestMapping(path = "/",method = RequestMethod.GET)
    public String home() {
        return "home";
    }

    @RequestMapping(path = "/users",method = RequestMethod.GET)
    public String users(Model model) {
        model.addAttribute("users",userService.findAll());
        return "userlist";
    }
}

-- Add role
INSERT INTO role(name) VALUES ('UNVERIFIED');
INSERT INTO role(name) VALUES ('ADMIN');
INSERT INTO role(name) VALUES ('MEMBER');

-- Add privilege
INSERT INTO privilege(name) VALUES ('UNVERIFIED');
INSERT INTO privilege(name) VALUES ('ADMIN');
INSERT INTO privilege(name) VALUES ('MEMBER');

-- Add role privilege association
INSERT INTO role_privilege(role_id,privilege_id) VALUES (1,1);
INSERT INTO role_privilege(role_id,privilege_id) VALUES (2,2);
INSERT INTO role_privilege(role_id,privilege_id) VALUES (3,3);
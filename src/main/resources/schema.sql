/*DROP TABLE IF EXISTS role_privilege,user_role,privilege,role,myuser;*/

CREATE TABLE IF NOT EXISTS myuser (
  user_id serial primary key,
  username varchar(100) NOT NULL UNIQUE,
  password varchar(100) NOT NULL,
  email varchar(100) NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS role (
  role_id serial primary key,
  name varchar(100) NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS privilege (
  privilege_id serial primary key,
  name varchar(100) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS user_role (
  user_id serial,
  role_id serial
);
ALTER TABLE user_role ADD FOREIGN KEY (user_id) REFERENCES myuser(user_id);
ALTER TABLE user_role ADD FOREIGN KEY (role_id) REFERENCES role(role_id);

CREATE TABLE IF NOT EXISTS role_privilege (
  role_id serial,
  privilege_id serial
);
ALTER TABLE role_privilege ADD FOREIGN KEY (role_id) REFERENCES role(role_id);
ALTER TABLE role_privilege ADD FOREIGN KEY (privilege_id) REFERENCES privilege(privilege_id);